import { Component, OnInit } from '@angular/core';
import { ingredient } from '../shared/ingredient.model';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit {
  ingredients: ingredient[] = [
    new ingredient('Cakes', 5),
    new ingredient('Apples', 12),
    new ingredient('Tomatoes', 6)
  ];

  constructor() { }

  ngOnInit() {
  }

}
